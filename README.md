[![](https://images.microbadger.com/badges/image/colisweb/node-aws-docker.svg)](https://microbadger.com/images/colisweb/node-aws-docker "Get your own image badge on microbadger.com")
[![](https://images.microbadger.com/badges/version/colisweb/node-aws-docker.svg)](https://microbadger.com/images/colisweb/node-aws-docker "Get your own version badge on microbadger.com")

Docker image to use in CI with node, AWS client, docker and git-secret

Published on [hub.docker.com/r/colisweb/node-aws-docker](https://hub.docker.com/r/colisweb/node-aws-docker)

Get it with

    docker pull colisweb/node-aws-docker:10-stretch